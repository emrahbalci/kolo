let modules = {}

modules.mobileMenu = () => {
    $(document).on('click', '.toggle-menu', function() {
        $(this).toggleClass('active');
        $('.main-menu').toggleClass('active');
    })
};

modules.formLabelAnimate = () => {
    $('.form-field input, .form-field textarea').on('keyup keydown', function () {
        if ($(this).val().length > 0)
            $(this).addClass('filled')
        else
            $(this).removeClass('filled')

    })
};

modules.fixHeader = () => {
    $(window).on('load scroll', function () {
        var $scrollTop = $(window).scrollTop();

        if ($scrollTop > 10) {
            $('#header').addClass('fixed');
        } else {
            $('#header').removeClass('fixed');
        }

    });
};

modules.fixHomepageBrands = () => {
    var $brandList = $('.brands-list .brands-item');
    var timeout = null;
    if ($brandList && $brandList.length > 0)
        $(window).on('load scroll', function () {
            var scrollTop = $(window).scrollTop();
            if ($(window).width() > 768) {
                clearTimeout(timeout);
                timeout = setTimeout(function () {
                    $brandList.each(function () {
                        var $this = $(this);
                        var top = $this.offset().top-100;
                        if (scrollTop > top && scrollTop < (top+$this.height()-100))
                            $(window).scrollTop(top);

                    });
                }, 150);
            }

            $brandList.each(function () {
                var $this = $(this);
                var top = $this.offset().top-250;
                if (scrollTop > top)
                    $(this).addClass('active');

            });

            var $brandAll = $('.brands-all-item');
            var allTop = $brandAll.offset().top-250;
            if (scrollTop > allTop)
                $brandAll.addClass('active');

        });
};


module.exports = modules
