{
    let $ = global.$ || {};
    let jQuery = global.jQuery || {};

    $ = jQuery = require('jquery');

    global.$ = $;
    global.jQuery = jQuery;

    require('jquery-validation');
    require('owl.carousel');

    const common = require('./utils/common.js');

    //mobile menu
    common.mobileMenu();

    //form field label animate
    common.formLabelAnimate();

    //fix header
    common.fixHeader();

    common.fixHomepageBrands();

    $('.homepage-slider').owlCarousel({
        loop: false,
        nav: false,
        dots: true,
        items: 1,
        smartSpeed: 1000,
        animateOut: 'fadeOut'
    });
}
